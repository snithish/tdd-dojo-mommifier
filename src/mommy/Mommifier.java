package mommy;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;

public class Mommifier {

    private static final List<String> vowels = unmodifiableList(asList("a", "e", "i", "o", "u", "A", "E", "I", "O", "U"));

    private Boolean cannotMommify(String input) {
        int vowelCount = 0;
        char[] inputChars = input.toCharArray();
        for (char character : inputChars) {
            String testChar = String.valueOf(character);
            if (vowels.contains(testChar)) {
                vowelCount++;
            }
        }
        Double percentage = (double) vowelCount / input.length();
        return percentage.compareTo(0.3) == -1;
    }

    public String mommify(String input) {
        if (cannotMommify(input)) {
            return input;
        }
        String[] inputCharacters = input.split("");
        StringBuilder result = new StringBuilder();
        boolean wasPreviousCharacterVowel = false;
        for (String character : inputCharacters) {
            String stringToBeAppended = stringToBeAppended(character, wasPreviousCharacterVowel);
            wasPreviousCharacterVowel = !stringToBeAppended.equals(character);
            result.append(stringToBeAppended);
        }
        return result.toString();
    }

    private String stringToBeAppended(String character, Boolean wasPreviousCharacterVowel) {
        if (vowels.contains(character)) {
            if (!wasPreviousCharacterVowel) {
                return "mommy";
            }
            return "";
        }
        return character;
    }
}
