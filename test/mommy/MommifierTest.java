package mommy;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MommifierTest {

    private Mommifier mommifier = new Mommifier();

    @Test
    public void shouldReturnEmptyStringWhenInputIsEmptyString() {
        String result = mommifier.mommify("");
        assertEquals("", result);
    }

    @Test
    public void shouldReturnJWhenInputIsJ() {
        Mommifier mommifier = new Mommifier();
        String result = mommifier.mommify("j");
        assertEquals("j", result);
    }

    @Test
    public void shouldReturnUppperCaseJWhenInputIsUpperCaseJ() {
        String result = mommifier.mommify("J");
        assertEquals("J", result);
    }

    @Test
    public void shouldReturnXWhenInputIsX() {
        String result = mommifier.mommify("X");
        assertEquals("X", result);
    }

    @Test
    public void shouldReturnMommyWhenInputIsA() {
        String result = mommifier.mommify("a");
        assertEquals("mommy", result);
    }

    @Test
    public void shouldReturnMommyWhenInputIsO() {
        String result = mommifier.mommify("o");
        assertEquals("mommy", result);
    }

    @Test
    public void shouldReturnMommyWhenInputIsE() {
        String result = mommifier.mommify("e");
        assertEquals("mommy", result);
    }

    @Test
    public void shouldReturnMommyWhenInputIsAO() {
        String result = mommifier.mommify("ao");
        assertEquals("mommy", result);
    }

    @Test
    public void shouldReturnMommyWWhenInputIsAW() {
        String result = mommifier.mommify("aw");
        assertEquals("mommyw", result);
    }

    @Test
    public void shouldReturnWSAMWhenInputIsWSAM() {
        String result = mommifier.mommify("wsam");
        assertEquals("wsam", result);
    }

    @Test
    public void shouldReturnMommySMommyWhenInputIsASA() {
        String result = mommifier.mommify("asa");
        assertEquals("mommysmommy", result);
    }

    @Test
    public void shouldReturnMommyWMommyCaseSensitiveWhenInputASA() {
        String result = mommifier.mommify("ASA");
        assertEquals("mommySmommy", result);
    }

    @Test
    public void shouldReturnHardWhenInputIsHard() {
        String result = mommifier.mommify("hard");
        assertEquals("hard", result);
    }

    @Test
    public void shouldReturnMommyMommyWhenInputIsMommyMommy() {
        String result = mommifier.mommify("mommymommy");
        assertEquals("mommymommy", result);
    }

    @Test
    public void shouldReturnMmommymmyMmommymmymommyWhenInputIsMommyMommyAEIOUAEIOUAEIOUAEIOUAEIOUAEIOUAEIOUAEIOUAEIOU() {
        String result = mommifier.mommify("MommyMommyAEIOUAEIOUAEIOUAEIOUAEIOUAEIOUAEIOUAEIOUAEIOU");
        assertEquals("MmommymmyMmommymmymommy", result);
    }

}
